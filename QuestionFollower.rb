class QuestionFollower
  attr_accessor :question_id, :user_id
  
  def self.all
    results = QuestionDatabase.instance.execute('SELECT * FROM question_followers')
    results.map { |result| QuestionFollower.new(result) }
  end
  
  def initialize(options = {} )
    @question_id = options['question_id']
    @user_id = options['user_id']
  end
  
  def find_by_id(qf_id)
    question_follower = QuestionDatabase.instance.execute(<<-SQL, qf_id)
    SELECT
    *
    FROM
    question_followers
    WHERE
    id = ?
    SQL
    QuestionFollower.new(question_follower.first)
  end
  
  def self.followers_for_question_id(question_id)
    followers = QuestionDatabase.instance.execute(<<-SQL, question_id)
    SELECT
    users.id
    FROM
    users
    JOIN
    question_followers
    ON users.id = user_id
    WHERE
    question_id = ?
    SQL
    followers.map {|user| User.find_by_id(user['id'])}
  end
  
  def self.followed_questions_for_user_id(user_id)
    questions = QuestionDatabase.instance.execute(<<-SQL, user_id)
    SELECT
    questions.id
    FROM
    questions
    JOIN
    question_followers
    ON questions.id = question_id
    WHERE
    questions.user_id = ?
    SQL
    questions.map {|question| Question.find_by_id(question['id'])}
  end
  
  def self.most_followed_questions(n)
    questions = QuestionDatabase.instance.execute(<<-SQL, n)
    SELECT
    question_id
    FROM
    question_followers
    GROUP BY question_id
    ORDER BY COUNT(*) DESC
    LIMIT ?
    SQL

    questions.map { |question| Question.find_by_id(question['question_id']) }
  end
  

end
