class Reply
  include DBSave
  attr_accessor :question_id, :reply_id, :user_id, :id, :body
  
  def self.all
    results = QuestionDatabase.instance.execute('SELECT * FROM replies')
    results.map { |result| Reply.new(result) }
  end
  
  def initialize(options = {})
    @question_id = options['question_id']
    @reply_id = options['reply_id']
    @user_id = options['user_id']
    @id = options['id']
    @body = options['body']
  end
  
  def self.find_by_id(reply_id)
    reply = QuestionDatabase.instance.execute(<<-SQL, reply_id)
    
    SELECT * 
    FROM replies
    WHERE id = ?
    
    SQL
    
    Reply.new(reply.first)
  end
  
  def find_by_user_id(user_id)
    replies = QuestionDatabase.instance.execute(<<-SQL, user_id)
    
    SELECT * 
    FROM replies
    WHERE user_id = ?
    
    SQL
    
    replies.map { |result| Reply.new(result) }
  end
  
  def author
    User::find_by_id(@user_id)
  end
  
  def question
    Question::find_by_id(@question_id)
  end
  
  def parent_reply
    Reply.find_by_id(@reply_id)
  end
  
  def child_replies
    replies = QuestionDatabase.instance.execute(<<-SQL, id)
    
    
    SELECT * 
    FROM replies
    WHERE reply_id = ?
    
    SQL
    replies.map { |result| Reply.new(result) }
  end
  
  def self.find_by_question_id(question_id)
    replies = QuestionDatabase.instance.execute(<<-SQL, question_id)
    
    SELECT * 
    FROM replies
    WHERE question_id = ?
    
    SQL
    
    replies.map { |reply| Reply.new(reply) }
  end
  
  def save
    save_method("replies")
  end
  

end