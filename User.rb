class User
  include DBSave
  attr_accessor :id, :fname, :lname, :followed_questions
  
  def self.all
    results = QuestionDatabase.instance.execute('SELECT * FROM users')
    results.map { |result| User.new(result) }
  end
  
  def initialize(options={})
    @id = options['id']
    @fname = options['fname']
    @lname = options['lname']
  end
  
  
  def ask_question(title, body)
    new_question = Question.new({ "title" => title, 
      "body" => body, "user_id" => @id })
    new_question.create
  end
    
  def like_question(question)
    new_like = QuestionLike.new( { "user_id" => @id, 
      "question_id" => question.id })
    new_like.create
  end
  
  def follow_question(question)
    question_id = question.id
    QuestionDatabase.instance.execute(<<-SQL, question_id, id)
    INSERT INTO
    question_followers(question_id,user_id)
    VALUES
    (?,?)
    SQL
  end
  
  def self.find_by_name(fname,lname)
    user = QuestionDatabase.instance.execute(<<-SQL, fname, lname)
    SELECT
    *
    FROM
    users
    WHERE
    fname = ? AND lname = ?
    SQL
    
    user.map { |user| User.new(user) }.first
  end
  
  def self.find_by_id(user_id)
    user = QuestionDatabase.instance.execute(<<-SQL, user_id)
    SELECT
    *
    FROM
    users
    WHERE
    id = ?
    SQL
    
    User.new(user.first)
  end
  
  def liked_questions
    QuestionLike.liked_questions_for_user_id(@id)
  end

  
  def authored_questions
    questions = QuestionDatabase.instance.execute(<<-SQL, id)
    SELECT
    *
    FROM
    questions
    WHERE
    user_id = ?
    SQL
    
    questions.map {|question| Question.new(question) }
  end
  
  def authored_replies
    Reply.find_by_user_id(@id)
  end
  
  def followed_questions
    QuestionFollower.followed_questions_for_user_id(@id)
  end
  
  def average_karma
    karma = QuestionDatabase.instance.execute(<<-SQL, id)
    
    /*Get total count of questions */
    SELECT CAST(
       SUM(CASE WHEN question_id IS NOT NULL THEN 1 ELSE 0 END)
       / (COUNT(DISTINCT question_id)) AS FLOAT) AS karma
    FROM questions
    LEFT OUTER JOIN question_likes
    ON questions.id = question_likes.question_id
    WHERE question_likes.user_id = ?
    
    SQL
    karma.first['karma']
    
  end
  
  def save
    save_method("users")
  end
end
