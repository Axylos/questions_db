class Question
  include DBSave
  attr_accessor :id, :title, :body, :user_id
  
  def self.all
    results = QuestionDatabase.instance.execute('SELECT * FROM questions')
    results.map { |result| Question.new(result) }
  end
  
  def initialize(options = {})
    @id = options['id']
    @title = options['title']
    @body = options['body']
    @user_id = options['user_id']
  end
  
  def author
    User::find_by_id(@user_id)
  end
  
  def replies
    Reply::find_by_question_id(@id)
  end

  
  def self.find_by_id(question_id)
    question = QuestionDatabase.instance.execute(<<-SQL, question_id)
    
    SELECT * 
    FROM questions
    WHERE id = ?
    
    SQL

    Question.new(question.first)
  end
  
  def self.find_by_author_id(author_id)
    author = User.find_by_id(author_id)
    author.authored_questions
  end
  
  def followers
    QuestionFollower.followers_for_question_id(@id)
  end
  
  def likers
    QuestionLike.likers_for_question_id(@id)
  end
  
  def num_likes
    QuestionLike.num_likes_for_question_id(@id)
  end
  
  def self.most_followed(n=1)
    QuestionFollower.most_followed_questions(n)
  end
  
  def self.most_liked(n=1)
    QuestionLike.most_liked_questions(n)
  end
  
  def save
    save_method("questions")
  end
  

end