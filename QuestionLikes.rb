class QuestionLike
  
  attr_reader :user_id, :question_id, :id
  
  def self.all
    results = QuestionDatabase.instance.execute('SELECT * FROM question_likes')
    results.map { |result| QuestionLike.new(result) }
  end
  
  def initialize(options={})
    @user_id = options['user_id']
    @question_id = options['question_id']
    @id = options['id']
  end
  
  def self.find_by_id(ql_id)
    question_like = QuestionDatabase.instance.execute(<<-SQL, ql_id)
    
    SELECT * 
    FROM question_likes
    WHERE id = ?
    
    SQL
    
    QuestionLike.new(question_like.first)
  end
  
  def self.likers_for_question_id(question_id)
    likers = QuestionDatabase.instance.execute(<<-SQL, question_id)
    
    SELECT
    user_id
    FROM
    question_likes
    WHERE
    question_id = ?
    
    SQL
    
    likers.map { |user_id| User.find_by_id(user_id['user_id']) }
  end
  
  def self.num_likes_for_question_id(question_id)
    num_likes = QuestionDatabase.instance.execute(<<-SQL, question_id)
    
    SELECT COUNT(*) AS num
    FROM
    question_likes
    WHERE question_id = ?
    GROUP BY 
    question_id
    
    SQL
    raise "No one likes this" if num_likes.empty?
    num_likes.first['num']
  end
  
  def self.liked_questions_for_user_id(user_id)
    liked_questions = QuestionDatabase.instance.execute(<<-SQL, user_id)
    
    SELECT question_id
    FROM question_likes
    WHERE user_id = ?
    
    SQL
    
    liked_questions.map { |likes| Question.find_by_id(likes['question_id']) }
  end
  
  def self.most_liked_questions(n)
    questions = QuestionDatabase.instance.execute(<<-SQL, n)
    
    SELECT
    question_id
    FROM
    question_likes
    GROUP BY
    question_id
    ORDER BY
    COUNT(*) DESC
    LIMIT ?
    
    SQL
    
    questions.map { |question| Question.find_by_id(question['question_id'])}
  end
  
  def create 
    raise ":(" unless self.id.nil?
    
    QuestionDatabase.instance.execute(<<-SQL, user_id, question_id)
    INSERT INTO
    question_likes(user_id, question_id)
    VALUES
    (?, ?)
    SQL
    
    @id = QuestionDatabase.instance.last_insert_row_id
  end
end