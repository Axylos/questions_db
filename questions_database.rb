require_relative './save.rb'
require 'singleton'
require 'sqlite3'
require './Reply.rb'
require_relative './User.rb'
require_relative './Question.rb'
require_relative './QuestionFollower.rb'
require_relative './QuestionLikes.rb'


class QuestionDatabase < SQLite3::Database
  include Singleton
  
  def initialize
    super('questions2.db')
    self.results_as_hash = true
    self.type_translation = true
  end
  
end





if $PROGRAM_NAME == __FILE__

  p Question.most_liked(2)
end
  

