module DBSave
  
  def save_method(tablename)
    vars = self.instance_variables.map(&:to_s)
    second_vars = vars.map { |var| var[1..-1].to_sym }
    second_vars.map! { |var| self.send(var) }
    
    
    
    i_vars = vars.map(&:to_s)
    i_vars.map! { |var| var[1..-1] }
    
    cols = i_vars.join(", ")
    
    set_vars = i_vars.map { |var| "#{var} = ?" }
    set_vars = set_vars.join(", ")
    
    insert_vals = vars.map { "?" }.join(", ")
    
    
    if self.id.nil?
      QuestionDatabase.instance.execute(<<-SQL, *second_vars)
      
      INSERT INTO
      #{tablename} 
      (#{cols})
      VALUES
      (#{insert_vals})
      SQL
    else
      QuestionDatabase.instance.execute(<<-SQL, *second_vars, self.id)
      UPDATE
      #{tablename}
      SET
      #{set_vars}
      WHERE
      id = ?
      SQL
      
    end
    
  end
  
end