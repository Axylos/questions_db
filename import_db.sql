CREATE TABLE users (
  id INTEGER PRIMARY KEY,
  fname varchar(255) NOT NULL, 
  lname varchar(255) NOT NULL
);

CREATE TABLE questions (
  user_id INTEGER NOT NULL,
  id INTEGER PRIMARY KEY,
  title varchar(255) NOT NULL,
  body text NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE question_followers(
  question_id INTEGER NOT NULL,
  user_id INTEGER NOT NULL,
  id INTEGER PRIMARY KEY,
  FOREIGN KEY (question_id) REFERENCES questions(id),
  FOREIGN KEY (user_id) REFERENCES users(id) 
);

CREATE TABLE replies (
  question_id INTEGER NOT NULL,
  reply_id INTEGER NOT NULL,
  user_id INTEGER NOT NULL,
  id INTEGER PRIMARY KEY,
  body text NOT NULL,
  FOREIGN KEY (question_id) REFERENCES questions(id),
  FOREIGN KEY (reply_id) REFERENCES replies(id),
  FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE question_likes (
  user_id INTEGER NOT NULL,
  question_id INTEGER NOT NULL,
  id INTEGER PRIMARY KEY,
  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (question_id) REFERENCES questions(id)
);

INSERT INTO
  users(fname,lname)
VALUES
  ('Albert', 'Einstein');
  
INSERT INTO 
  questions(title,body,user_id)
VALUES
('What is the speed of light?', 'Hey can someone help me with E=MC2?', 1);



